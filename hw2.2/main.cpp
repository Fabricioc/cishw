#include <iostream>
#include <bits/stdc++.h>
using namespace std;
double GetMedian(int array[], int arraylength) {
  double* dpSorted = new double[arraylength];
	for (int i = 0; i < arraylength; ++i) {
    	dpSorted[i] = array[i];
	}
	for (int i = arraylength - 1; i > 0; --i) {
    	for (int j = 0; j < i; ++j) {
        	if (dpSorted[j] > dpSorted[j+1]) {
            	double dTemp = dpSorted[j];
            	dpSorted[j] = dpSorted[j+1];
            	dpSorted[j+1] = dTemp;
        	}
    	}
    	double Median;
	if ((arraylength % 2) != 0) {
    	Median = (dpSorted[arraylength/2] + dpSorted[(arraylength/2) - 1])/2.0;
	} else {
    	Median = dpSorted[arraylength/2];
	}
   	 
	delete [] dpSorted;
	return Median;
}
	}
 

double GetMode(int array[], int arraylength ) {
   
	int* ipRepetition = new int[arraylength];
	for (int i = 0; i < arraylength; ++i) {
    	ipRepetition[i] = 0;
    	int j = 0;
    	// what kinda filter is this again?
    	while ((j < i) && (array[i] != array[j])) {
        	if (array[i] != array[j]) {
            	++j;
        	}
    	}
    	++(ipRepetition[j]);
	}
	int iMaxRepeat = 0;
	for (int i = 1; i < arraylength; ++i) {
    	if (ipRepetition[i] > ipRepetition[iMaxRepeat]) {
        	iMaxRepeat = i;
    	}
       	}
    
    	delete [] ipRepetition;
	return array[iMaxRepeat];
}

double GetMean(int array[], int arraylength) {
	double dSum = array[0];
	for (int i = 1; i < arraylength; ++i) {
    	dSum += array[i];
	}
        	return dSum/arraylength;
   
}

 void getave( int array[], int arraylength , float sum )
{
 	float avg;
	int counter = 0;
	for (int pass = 0; pass < arraylength - 1; pass++)
    	for (int count = pass + 1; count < arraylength; count++) {
        	if (array [count] == array [pass])
            	counter++;
 	avg = sum/arraylength;
    	}
   
	 
	cout << "the average is *" ;
	cout<< avg <<endl;    
    
}

int main()
{
	int  arraylength , sum;	 
    
	cout << "how many students : ";
	cin >> arraylength;
    
	int* array = new int[arraylength];

	cout << "Enter how many movies:" << endl;
	for ( int i = 0; i < arraylength; i++ )
	{
    	cout << "student" << i+1 << " = ";
    	cin >> array[i];
    	sum += array[i];
	}
    
	getave(array, arraylength, sum);
	GetMode( array, arraylength);
 	GetMedian(array, arraylength);
 	GetMean( array, arraylength);
	cout << "Mode = *" << GetMode( array,  arraylength ) <<endl;
	cout << "the median is*"<< GetMedian(array, arraylength) <<endl;
	cout << "the mean is*"  << GetMean(array, arraylength)<<endl;
 
	delete[] array;
}
