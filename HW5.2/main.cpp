#include <cstdlib>

#include <ctime>

#include <iostream> 

using namespace std;

class Coin

{
private:
	string sideUp;

public:

	// randomly initialize the sideUp data member

	// by re-using the logic in toss

	Coin()

	{

		srand(time(0)); // initialize the random number

         }

	~Coin()

	{

	}

	// generate a random number

	//   evens will be assigned to heads

	//   odds will be assigned to tails

	void toss()

	{

		if (rand() % 2 == 0)

			sideUp = "heads";

		else

			sideUp = "tails";

	}
	string getSideUp()

	{

		return sideUp;

	}

};

int main(int argc, char *argv[])

{

	Coin coin;      

	int heads = 0;

	int tails = 0;

	

	// Toss the coin 20x

	for (int i=0; i<20; ++i)

	{

		coin.toss();

		cout << "(" << (i+1) << ") Next toss result: " << coin.getSideUp() << endl;

		if (coin.getSideUp() == "heads")

			++heads;

		else

			++tails;

	}

	cout << "Number of times heads is up: " << heads << endl;

	cout << "Number of times tails is up: " << tails << endl;

	return 0;

}