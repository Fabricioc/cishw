/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 2, 2019, 2:16 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class Day{
private:
    string month;
    int day;
public:
Day();
Day(int d);

int getDay() const {
    
    return day;
    }
void setDay(int d){
    day= d;
}
void print(){
    cout<< month<<" "<<day<<endl;


}
        
};
Day::Day(int d){
    day = d;
	if (day <= 31) {
		month = "January";
		
	}
	else if (day > 31 && day <= 59) {
		month = "February";
	
	}
	else if (day > 59 && day <= 90) {
		month = "March";
		
	}
	else if (day > 90 && day <= 120) {
		month = "April";
		
	}
	else if (day > 120 && day <= 151) {
		month = "May";
		
	}
	else if (day > 151 && day <= 181) {
		month = "June";
		
	}
	else if (day > 181 && day <= 212) {
		month = "July";
		;
	}
	else if (day > 212 && day <= 243) {
		month = "August";
		
	}
	else if (day > 243 && day <= 273) {
		month = "September";
		
	}
	else if (day > 273 && day <= 304) {
		month = "October";
		
	}
	else if (day > 304 && day <= 334) {
		month = "November";
		
	}
	else if (day > 334 && day <= 365) {
		month = "December";
		;
	}
}
int main(int argc, char** argv) {

    int day;
	cout << "What day of the year do you want to see? : ";
	cin >> day;

	Day constructore(day);
    
    constructore.print();
    
    return 0;
}

