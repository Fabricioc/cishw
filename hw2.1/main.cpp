#include <iostream>
using namespace std;

 void reverse_array( int array[], int arraylength )
{
    for (int i = 0; i < (arraylength / 2); i++) {
        int temporary = array[i];                
        array[i] = array[(arraylength - 1) - i];
        array[(arraylength - 1) - i] = temporary;
    }
}

int main()
{
    int  arraylength;     
    
    cout << "Enter the array size: ";
    cin >> arraylength;
    
    int* array = new int[arraylength];

    cout << "Enter the numbers in the array:" << endl;
    for ( int i = 0; i < arraylength; i++ ) {
        cout << "Array[" << i << "] = ";
        cin >> array[i];
    }
    
    reverse_array( array, arraylength );
    
    cout << "The new array order is " << endl;
    for ( int i = 0; i < arraylength; i++ ) { 
        cout << "Array[" << i << "] = " << array[i] << endl;
    }
    
    delete[] array;
}

